#+title: Chapter 2 --- Building Abstractions with Data

* Introduction to Data Abstraction

** Example: Arithmetic Operations for Rational Numbers

** Abstraction Barriers

** What Is Meant by Data?

** Extended Exercise: Interval Arithmetic

* Hierarchical Data and the Closure Property

** Representing Sequences

** Hierarchical Structures

** Sequences as Conventional Interfaces

** Example: A Picture Language

* Symbolic Data

** Quotation

** Example: Symbolic Differentiation

** Example: Representing Sets

** Example: Huffman Encoding Trees

* Multiple Representations for Abstract Data

** Representations for Complex Numbers

** Tagged data

** Data-Directed Programming and Additivity

* Systems with Generic Operations

** Generic Arithmetic Operations

** Combining Data of Different Types

** Example: Symbolic Algebra

