#+title: Chapter 3 --- Modularity, Objects, and State

* Assignment and Local State

** Local State Variables

** The Benefits of Introducing Assignment

** The Costs of Introducing Assignment

* The Environment Model of Evaluation

** The Rules for Evaluation

** Applying Simple Procedures

** Frames as the Repository of Local State

** Internal Definitions

* Modeling with Mutable Data

** Mutable List Structure

** Representing Queues

** Representing Tables

** A Simulator for Digital Circuits

** Propagation of Constraints

* Concurrency: Time Is of the Essence

** The Nature of Time in Concurrent Systems

** Mechanisms for Controlling Concurrency

* Streams

** Streams Are Delayed Lists

** Infinite Streams

** Exploiting the Stream Paradigm

** Streams and Delayed Evaluation

** Modularity of Functional Programs and Modularity of Objects
