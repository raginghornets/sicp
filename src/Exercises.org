#+title: My SICP Exercise Solutions
#+description: airicbear's solutions for the included exerises in the book, Structure and Interpretation of Computer Programs (SICP)

* Chapter 1

** Exercise 1.1

*** Description

    Below is a sequence of expressions.
    What is the result printed by the interpreter in response to each expression?
    Assume that the sequence is to be evaluated in the order in which it is presented.

    #+begin_example scheme
      10
      (+ 5 3 4)
      (- 9 1)
      (/ 6 2)
      (+ (* 2 4) (- 4 6))
      (define a 3)
      (define b (+ a 1))
      (+ a b (* a b))
      (= a b)
      (if (and (> b a) (< b (* a b)))
          b
          a)

      (cond ((= a 4) 6)
            ((= b 4) (+ 6 7 a))
            (else 25))

      (+ 2 (if (> b a) b a))

      (* (cond ((> a b) a)
               ((< a b) b)
               (else -1))
         (+ a 1))
    #+end_example

*** Solution

    #+begin_src scheme :results both :exports both :session chapter1
      10
    #+end_src

    #+results:
    : 10

    #+begin_src scheme :results both :exports both :session chapter1
      (+ 5 3 4)
    #+end_src

    #+results:
    : 12

    #+begin_src scheme :results both :exports both :session chapter1
      (- 9 1)
    #+end_src

    #+results:
    : 8

    #+begin_src scheme :results both :exports both :session chapter1
      (/ 6 2)
    #+end_src

    #+results:
    : 3

    #+begin_src scheme :results both :exports both :session chapter1
      (+ (* 2 4) (- 4 6))
    #+end_src

    #+results:
    : 6

    #+begin_src scheme :results both :exports both :session chapter1
      (define a 3)
    #+end_src

    #+results:
    : a

    #+begin_src scheme :results both :exports both :session chapter1
      (define b (+ a 1))
    #+end_src

    #+results:
    : b

    #+begin_src scheme :results both :exports both :session chapter1
      (+ a b (* a b))
    #+end_src

    #+results:
    : 19

    #+begin_src scheme :results both :exports both :session chapter1
      (= a b)
    #+end_src

    #+results:
    : #f

    #+begin_src scheme :results both :exports both :session chapter1
      (if (and (> b a) (< b (* a b)))
          b
          a)
    #+end_src

    #+results:
    : 4

    #+begin_src scheme :results both :exports both :session chapter1
      (cond ((= a 4) 6)
            ((= b 4) (+ 6 7 a))
            (else 25))
    #+end_src

    #+results:
    : 16
 
    #+begin_src scheme :results both :exports both :session chapter1
      (+ 2 (if (> b a) b a))
    #+end_src

    #+results:
    : 6

    #+begin_src scheme :results both :exports both :session chapter1
      (* (cond ((> a b) a)
               ((< a b) b)
               (else -1))
         (+ a 1))
    #+end_src

    #+results:
    : 16

** Exercise 1.2

*** Description

    Translate the following expression into prefix form:

    $$\frac{5 + 4 + (2 - (3 - (6 + \frac{4}{5})))}{3 (6 - 2) (2 - 7)}$$

*** Solution

    #+begin_src scheme :results both :exports both :session chapter1
      (/ (+ 5
            4
            (- 2
               (- 3
                  (+ 6
                     (/ 4 5)))))
         (* 3
            (- 6 2)
            (- 2 7)))
    #+end_src

    #+results:
    : -37/150

** Exercise 1.3

*** Description

    Define a procedure that takes three numbers as arguments
    and returns the sum of the squares of the two larger numbers.

*** Solution

    #+begin_src scheme :results both :exports both :session chapter1
      (define (square x)
        (* x x))

      (define (sum-of-squares x y)
        (+ (square x) (square y)))

      (define (sum-of-squares-of-two-larger-numbers x y z)
        (cond ((and (<= x y) (<= x z)) (sum-of-squares y z))
              ((and (<= y x) (<= y z)) (sum-of-squares x z))
              ((and (<= z x) (<= z y)) (sum-of-squares x y))))

      (sum-of-squares-of-two-larger-numbers 3 3 3)
    #+end_src

    #+results:
    : 18

** Exercise 1.4

*** Description

    Observe that our model of evaluation allows for combinations whose operators are compound expressions.
    Use this observation to describe the behavior of the following procedure:

    #+begin_src scheme :results both :exports both :session chapter1
      (define (a-plus-abs-b a b)
        ((if (> b 0) + -) a b))
    #+end_src

*** Solution

    The procedure, =a-plus-abs-b= is defined as an combination whose operator is a compound expression.
    That compound expression uses a predicate, =(> b 0)=, within the context of a special form,
    =if=, to determine whether it will produce a =+= or a =-= operator.
    The resulting operator is then used to evaluate the original combination of the procedure.

** Exercise 1.5

*** Description

    Ben Bitdiddle has invented a test to determine whether the interpreter he is faced
    with is using applicative-order evaluation or normal-order evaluation.
    He defines the following two procedures:

    #+begin_src scheme :results both :exports both :session chapter1
      (define (p) (p))
      (define (test x y)
        (if (= x 0) 0 y))
    #+end_src

    Then he evaluates the expression

    #+begin_src scheme :results both :exports both :session chapter1
      (test 0 (p))
    #+end_src

    #+results:

    What behavior will Ben observe with an interpreter that uses applicative-order evaluation?
    What behavior will he observe with an interpreter that uses normal-order evaluation?
    Explain your answer.
    (Assume that the evaluation rule for the special form =if= is the same whether
    the interpreter is using normal or applicative order:
    The predicate expression is evaluated first, and the result determines whether
    to evaluate the consequent or the alternative expression.)

*** Solution

    In an interpreter that uses applicative-order evaluation, Ben will observe that
    the interpreter will freeze due to a recursion error since =(test 0 (p))= would
    evaluate all of its arguments, including the infinitely deep recursion procedure, =(p)=.

    In an interpreter that uses normal-order evaluation, =(p)= would not be evaluated until
    necessary, thus preventing the issue that arises with the applicative-order evaluation
    in this case.

** Exercise 1.6

*** Description

    Alyssa P. Hacker doesn't see why =if= needs to be provided as a special form.
    "Why can't I just define it as an ordinary procedure in terms of =cond=?" she asks.
    Alyssa's friend Eva Lu Ator claims this can indeed be done, and defines a new version of =if=:

    #+begin_src scheme :results both :exports both :session chapter1
      (define (new-if predicate then-clause else-clause)
        (cond (predicate then-clause)
              (else else-clause)))
    #+end_src

    Eva demonstrates the program for Alyssa:

    #+begin_src scheme :results both :exports both :session chapter1
      (new-if (= 2 3) 0 5)
    #+end_src

    #+results:

    #+begin_src scheme :results both :exports both :session chapter1
      (new-if (= 1 1) 0 5)
    #+end_src

    #+results:

    Delighted, Alyssa uses =new-if= to rewrite the square-root program:

    #+begin_src scheme :results both :exports both :session chapter1
      (define (sqrt-iter guess x)
        (new-if (good-enough? guess x)
                guess
                (sqrt-iter (improve guess x) x)))
    #+end_src

    What happens when Alyssa attempts to use this to compute square roots?
    Explain.

*** Solution

    Alyssa will find that since =(new-if)= is an ordinary procedure and the Scheme interpreter
    uses applicative-order evaluation, the expression =(sqrt-iter (improve guess x) x)))= will
    be stuck in an infinite recursion loop, thus its evaluation will never terminate.

** Exercise 1.7

*** Description

    The =good-enough?= test used in computing square roots will not be very effective for finding
    the square roots of very small numbers.
    Also, in real computers, arithmetic operations are almost always performed with limited
    precision.
    This makes our test inadequate for very large numbers.
    Explain these statements, with examples showing how the test fails for small and large numbers.
    An alternative strategy for implementing =good-enough?= is to watch how =guess= changes from
    one iteration to the next and to stop when the change is a very small fraction of the guess.
    Design a square-root procedure that uses this kind of end test.
    Does this work better for small and large numbers?

*** Solution

    Our current =good-enough?= procedure

    #+begin_src scheme :results both :exports both :session chapter1
      (define (good-enough? guess x)
        (< (abs (- (square guess) x)) 0.001))
    #+end_src

    is inadequate for very small or large numbers because it relies on a precision based algorithm
    with a manually predetermined precision threshold.
    We can see how =good-enough?= evaluates to true too early or too late
    in the following examples,

    #+begin_src scheme :results both :exports both :session chapter1
      (good-enough? 0.01 0.000043214321)
    #+end_src

    #+results:

    #+begin_src scheme :results both :exports both :session chapter1
      (sqrt 0.000043214321)
    #+end_src

    #+results:
    : 6.573760035170131e-3

    #+begin_src scheme :results both :exports both :session chapter1
      (good-enough? 11110861.10579846 123451234512345)
    #+end_src

    #+results:

    #+begin_src scheme :results both :exports both :session chapter1
      (sqrt 123451234512345)
    #+end_src

    #+results:
    : Fails to produce a result in a timely manner.

    Instead, if the precision were to be automatically determined based on the magnitude of the
    radicand or, as suggested in the exercise, the precision were to be ignored and the algorithm
    relying on the size of differences between guesses then =good-enough?= could perform better
    for larger and smaller numbers.

    My alternative solution for the =good-enough?= test is as follows:

    #+begin_src scheme :results both :exports both :session chapter1
      (define (new-good-enough? previous-guess guess x)
        (< (abs (- (square guess) (square previous-guess))) 0.000000001))

      (define (improve guess x)
        (average guess (/ x guess)))

      (define (average x y)
        (/ (+ x y) 2))

      (define (new-sqrt-iter previous-guess guess x)
        (if (new-good-enough? previous-guess guess x)
            guess
            (new-sqrt-iter guess (improve guess x) x)))

      (define (new-sqrt x)
        (new-sqrt-iter 1.0 (improve 1.0 x) x))
    #+end_src

    #+results:
    : new-sqrt

    Now for comparison between the previous solution and my solution,

    #+begin_src scheme :results both :exports both :session chapter1
      (new-sqrt 0.000043214321)
    #+end_src

    #+results:
    : 6.573760035196727e-3

    #+begin_src scheme :results both :exports both :session chapter1
      (new-sqrt 123451234512345)
    #+end_src

    #+results:
    : 11110861.10579846

    To come up with my solution, I initially considered that I would need to add an additional
    parameter, =previous-guess=, if I were to compare different guesses.
    However, after seeing online solutions, I realized this could be further reduced so that
    =new-good-enough?= takes the same number of parameters as it originally would by replacing
    =previous-guess= with =(improve 1.0 x)= within the procedure definition of
    =new-good-enough?= as opposed to within =new-sqrt=.

** Exercise 1.8

*** Description

    Newton's method for cube roots is based on the fact that if $y$ is an approximation
    to the cube root of $x$, then a better approximation is given by the value

    $$\frac{x/y^2 + 2y}{3}.$$

    Use this formula to implement a cube-root procedure analogous to the square-root procedure.
    (In Section 1.3.4 we will see how to implement Newton's method in general as an abstraction
    of these square-root and cube-root procedures.)

*** Solution

    For my solution, it was fairly straightforward.
    The only problem I encountered was forgetting to change =square= to =cube=
    in the =cbrt-good-enough?= test.

    #+begin_src scheme :results both :exports both :session chapter1
      (define (cube x)
        (* x x x))

      (define (cbrt-good-enough? previous-guess guess x)
        (< (abs (- (cube guess) (cube previous-guess))) 0.0000000001))

      (define (cbrt-improve guess x)
        (/ (+ (/ x (square guess))
              (* 2 guess))
           3))

      (define (cbrt-iter previous-guess guess x)
        (if (cbrt-good-enough? previous-guess guess x)
            guess
            (cbrt-iter guess (cbrt-improve guess x) x)))

      (define (cbrt x)
        (cbrt-iter 1.0 (cbrt-improve 1.0 x) x))
    #+end_src

    #+RESULTS:

    #+begin_src scheme :results both :exports both :session chapter1
      (cbrt 8)
    #+end_src

    #+results:
    : 2

** Exercise 1.9
   
*** Description

    Each of the following two procedures defines a method for adding two positive integers
    in terms of the procedures =inc=, which increments its argument by 1, and =dec=, which
    decrements its argument by 1.

    #+begin_src scheme :results none :exports code :session chapter1 :eval no
      (define (+ a b)
        (if (= a 0) b (inc (+ (dec a) b))))
      (define (+ a b)
        (if (= a 0) b (+ (dec a) (inc b))))
    #+end_src

    Using the substitution model, illustrate the process generated by each procedure in evaluating
    =(+ 4 5)=.
    Are these processes iterative or recursive?

*** Solution

**** First procedure

     #+begin_src scheme :results none :exports code :session chapter1 :eval no
       (+ 4 5)
       (inc (+ 3 5))
       (inc (inc (+ 2 5)))
       (inc (inc (inc (+ 1 5))))
       (inc (inc (inc (inc (+ 0 5)))))
       (inc (inc (inc (inc 5))))
       (inc (inc (inc 6)))
       (inc (inc 7))
       (inc 8)
       9
     #+end_src

     This process is recursive.
     It expands then contracts.

**** Second procedure

     #+begin_src scheme :results none :exports code :session chapter1 :eval no
       (+ 4 5)
       (+ 3 6)
       (+ 2 7)
       (+ 1 8)
       (+ 0 9)
       9
     #+end_src

     This process is iterative.
     It doesn't change shape.

** Exercise 1.10

*** Description

    The following procedure computes a mathematical function called Ackermann's function.

    #+begin_src scheme :results none :exports code :session chapter1 :eval no
      (define (A x y)
        (cond ((= y 0) 0)
              ((= x 0) (* 2 y))
              ((= y 1) 2)
              (else (A (- x 1) (A x (- y 1))))))
        #+end_src

    What are the values of the following expressions?

    #+begin_example
      (A 1 10)
      (A 2 4)
      (A 3 3)
    #+end_example

    Consider the following procedures, where =A= is the procedure defined above:

    #+begin_src scheme :results none :exports code :session chapter1 :eval no
      (define (f n) (A 0 n))
      (define (g n) (A 1 n))
      (define (h n) (A 2 n))
      (define (k n) (* 5 n n))
    #+end_src

    Give concise mathematical definitions for the functions computed by
    the procedures =f=, =g=, and =h= for positive integer values of $n$.
    For example, =(k n)= computes $5n^2$.

*** Solution

**** Values of the expressions

***** =(A 1 10)=

      #+begin_src scheme :results none :exports code :session chapter1 :eval no
        (A 1 10)
        (A 0 (A 1 9))
        (A 0 (A 0 (A 1 8)))
        (A 0 (A 0 (A 0 (A 1 7))))
        (A 0 (A 0 (A 0 (A 0 (A 1 6)))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 5))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 4)))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 3))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 2)))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 1))))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 2)))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 4))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 8)))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 16))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 32)))))
        (A 0 (A 0 (A 0 (A 0 64))))
        (A 0 (A 0 (A 0 128)))
        (A 0 (A 0 256))
        (A 0 512)
        1024
      #+end_src

***** =(A 2 4)=

      #+begin_src scheme :results none :exports code :session chapter1 :eval no
        (A 2 4)
        (A 1 (A 2 3))
        (A 1 (A 1 (A 2 2)))
        (A 1 (A 1 (A 1 (A 1 1))))
        (A 1 (A 1 (A 1 2)))
        (A 1 (A 1 (A 0 (A 1 1))))
        (A 1 (A 1 (A 0 2)))
        (A 1 (A 1 4))
        (A 1 (A 0 (A 1 3)))
        (A 1 (A 0 (A 0 (A 1 2))))
        (A 1 (A 0 (A 0 (A 0 (A 1 1)))))
        (A 1 (A 0 (A 0 (A 0 2))))
        (A 1 (A 0 (A 0 4)))
        (A 1 (A 0 8))
        (A 1 16)
        (A 0 (A 1 15))
        (A 0 (A 0 (A 1 14)))
        (A 0 (A 0 (A 0 (A 1 13))))
        (A 0 (A 0 (A 0 (A 0 (A 1 12)))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 11))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 10)))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 9))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 8)))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 7))))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 6)))))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 5))))))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 4)))))))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 3))))))))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 2)))))))))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 1))))))))))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 2)))))))))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 4))))))))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 8)))))))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 16))))))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 32)))))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 64))))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 128)))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 256))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 512)))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 1024))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 2048)))))
        (A 0 (A 0 (A 0 (A 0 4096))))
        (A 0 (A 0 (A 0 8192)))
        (A 0 (A 0 16384))
        (A 0 32768)
        65536
      #+end_src

***** =(A 3 3)=

      #+begin_src scheme :results none :exports code :session chapter1 :eval no
        (A 3 3)
        (A 2 (A 3 2))
        (A 2 (A 2 (A 3 1)))
        (A 2 (A 2 2))
        (A 2 (A 1 (A 2 1)))
        (A 2 (A 1 2))
        (A 2 (A 0 (A 1 1)))
        (A 2 (A 0 2))
        (A 2 4)
        (A 1 (A 2 3))
        (A 1 (A 1 (A 2 2)))
        (A 1 (A 1 (A 1 (A 2 1))))
        (A 1 (A 1 (A 1 2)))
        (A 1 (A 1 (A 0 (A 1 1))))
        (A 1 (A 1 (A 0 2)))
        (A 1 (A 1 4))
        (A 1 (A 0 (A 1 3)))
        (A 1 (A 0 (A 0 (A 1 2))))
        (A 1 (A 0 (A 0 (A 0 (A 1 1)))))
        (A 1 (A 0 (A 0 (A 0 2))))
        (A 1 (A 0 (A 0 4)))
        (A 1 (A 0 8))
        (A 1 16)
        (A 0 (A 1 15))
        (A 0 (A 0 (A 1 14)))
        (A 0 (A 0 (A 0 (A 1 13))))
        (A 0 (A 0 (A 0 (A 0 (A 1 12)))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 11))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 10)))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 9))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 8)))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 7))))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 6)))))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 5))))))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 4)))))))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 3))))))))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 2)))))))))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 1))))))))))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 2)))))))))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 4))))))))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 8)))))))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 16))))))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 32)))))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 64))))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 128)))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 256))))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 512)))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 1024))))))
        (A 0 (A 0 (A 0 (A 0 (A 0 2048)))))
        (A 0 (A 0 (A 0 (A 0 4096))))
        (A 0 (A 0 (A 0 8192)))
        (A 0 (A 0 16384))
        (A 0 32768)
        65536
      #+end_src

**** Mathematical definitions

***** =f=

      =(f n)= computes $2n$.

***** =g=

      =(g n)= computes $2^n$.

***** =h=

      =(h n)= computes $2^{2^{\dots}}$ $n$ times.

***** =k=

      =(k n)= computes $5n^2$.
