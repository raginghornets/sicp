;; Prevent org-babel from executing all of the code blocks
(setq org-export-use-babel nil)

(defun sicp-sitemap-format-entry (entry _style project)
  "Return string for each ENTRY in PROJECT."
  (when (s-starts-with-p "Chapter" entry)
    (format "@@html:<span class=\"archive-item\">@@[[file:%s][%s]]@@html:</span>@@"
            entry
            (org-publish-find-title entry project))))

(defun sicp-sitemap-function (title list)
  "Return sitemap using TITLE and LIST returned by `sicp-sitemap-format-entry'."
  (concat "#+title: " title "\n"
          "\n#+begin_archive\n"
          "@@html:<ol>@@\n"
          (mapconcat (lambda (li)
                       (format "@@html:<li>@@%s@@html:</li>@@" (car li)))
                     (seq-filter #'car (cdr list))
                     "\n")
          "\n@@html:</ol>@@"
          "\n#+end_archive\n"))

(defvar sicp-nav
  (concat "<nav>"
          "<p>"
          "Navigation: "
          "<a href=\"index.html\">Home</a> "
          "<a href=\"sitemap.html\">Sitemap</a> "
          "<a href=\"Chapter01.html\">Chapter 1</a> "
          "<a href=\"Chapter02.html\">Chapter 2</a> "
          "<a href=\"Chapter03.html\">Chapter 3</a> "
          "<a href=\"Chapter04.html\">Chapter 4</a> "
          "<a href=\"Chapter05.html\">Chapter 5</a> "
          "<a href=\"Exercises.html\">Exercises</a> "
          "</nav>"))

(setq org-publish-project-alist
      `(("orgfiles"
         :base-directory "~/Workspace/sicp/src/"
         :recursive t
         :publishing-directory "~/Workspace/sicp/docs/"
         :publishing-function org-html-publish-to-html

         :lang "en"
         :with-toc t
         :title "Home"
         :email "tuk94307@temple.edu"
         :with-headline-numbers t
         :with-date t
         :with-title t
         :time-stamp-file t
         :section-numbers t
         :table-of-contents t
         :with-email t
         :with-smart-quotes t

         :html-doctype "html5"
         :html-head-include-scripts nil
         :html-head-include-default-style t
         :html-html5-fancy t
         :html-validation-link nil
         ;; :html-head ,sicp-head
         :html-preamble ,sicp-nav
         ;; :style ,sicp-css

         ;; Sitemap settings
         :auto-sitemap nil
         :sitemap-filename "sitemap.org"
         :sitemap-title "Notes by chapter"
         :sitemap-style list
         :sitemap-format-entry sicp-sitemap-format-entry
         :sitemap-function sicp-sitemap-function
         )))

;; Graphviz stuff
(defun gv-define-nodes (table shape fontname)
  (mapcar #'(lambda (x)
              (princ (format "%s [label=\"%s\", shape=\"%s\", fontname=\"%s\"];\n"
                             (first x)
                             (second x)
                             shape
                             fontname)))
          table))

(defun gv-define-monospace-nodes (table)
  (gv-define-nodes table "plaintext" "monospace"))

(defun gv-edge (a b)
  (princ (format "%s -- %s;\n" a b)))

(defun gv-edge-list (a b)
  (princ (format "%s -- {%s};\n" a b)))

(defun gv-node (n table)
  (first (nth n table)))

(defun gv-nodes (ns table)
  (mapconcat 'identity
             (mapcar (lambda (x) (node x)) ns)
             " "))
