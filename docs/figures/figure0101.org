#+name: eval-tree-table
#+attr_html: display="none"
| a | 390 |
| b |   * |
| c |  26 |
| d |  15 |
| e |   + |
| f |   2 |
| g |  24 |
| h |   + |
| i |   3 |
| j |   5 |
| k |   7 |
| l |   * |
| m |   4 |
| n |   6 |

#+name: eval-tree-dot
#+begin_src emacs-lisp :var table=eval-tree-table :results output :exports none
  (defun node (n)
    (gv-node n table))

  (defun nodes (ns)
    (gv-nodes ns table))

  (gv-define-monospace-nodes table)

  (gv-edge-list (node 0)
                (nodes '(1 2 3)))

  (gv-edge-list (node 2)
                (nodes '(4 5 6)))

  (gv-edge-list (node 3)
                (nodes '(7 8 9 10)))

  (gv-edge-list (node 6)
                (nodes '(11 12 13)))
#+end_src

#+RESULTS: eval-tree-dot
#+begin_example
a [label="390", shape="plaintext", fontname="monospace"];
b [label="*", shape="plaintext", fontname="monospace"];
c [label="26", shape="plaintext", fontname="monospace"];
d [label="15", shape="plaintext", fontname="monospace"];
e [label="+", shape="plaintext", fontname="monospace"];
f [label="2", shape="plaintext", fontname="monospace"];
g [label="24", shape="plaintext", fontname="monospace"];
h [label="+", shape="plaintext", fontname="monospace"];
i [label="3", shape="plaintext", fontname="monospace"];
j [label="5", shape="plaintext", fontname="monospace"];
k [label="7", shape="plaintext", fontname="monospace"];
l [label="*", shape="plaintext", fontname="monospace"];
m [label="4", shape="plaintext", fontname="monospace"];
n [label="6", shape="plaintext", fontname="monospace"];
a -- {b c d};
c -- {e f g};
d -- {h i j k};
g -- {l m n};
#+end_example

#+begin_src dot :file figure0101.png :var input=eval-tree-dot :exports results
  graph {
    $input
  }
#+end_src

#+RESULTS:
[[file:figure0101.png]]
